import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, SimpleGrid, Image } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import { customTheme } from '@utilities/theme';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <Box bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
      <VStack spacing="8">
        <SimpleGrid columns={{ base: 1, md: 2 }} spacing={10} alignItems="center" w="full">
          <Box>
            <BrandLogo />
            <Heading as="h1" size="2xl" textAlign={{ base: 'center', md: 'left' }}>
              Marko Kraemer
            </Heading>
            <Text fontSize={{ base: 'md', lg: 'lg' }} textAlign={{ base: 'center', md: 'left' }}>
              CEO & Co-Founder at SoftGen.ai
            </Text>
            <Button colorScheme="blue" size="lg" mt="6">
              Learn More
            </Button>
          </Box>
          <Box>
            <Image src="https://picsum.photos/400/300" borderRadius="lg" />
          </Box>
        </SimpleGrid>
        <VStack spacing="6" maxW="2xl" textAlign="center">
          <Heading as="h2" size="xl">
            Achievements
          </Heading>
          <Text fontSize="lg">
            From scaling a software development agency to 1+ million euros in revenue to pioneering in Generative AI, Marko has been at the forefront of innovation.
          </Text>
        </VStack>
        <Button colorScheme="green" size="lg">
          Get Started
        </Button>
      </VStack>
    </Box>
  );
};

export default Home;
