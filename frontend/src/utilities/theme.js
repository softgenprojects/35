import { extendTheme } from '@chakra-ui/react';

export const theme = extendTheme({
  colors: {
    primary: '#1A202C',
    secondary: '#2D3748',
    accent: '#3182CE',
    background: '#E2E8F0',
    text: '#2D3748',
    subtext: '#718096'
  },
  fonts: {
    heading: 'Arial, sans-serif',
    body: 'Arial, sans-serif',
    mono: 'Menlo, monospace'
  },
  styles: {
    global: {
      'html, body': {
        color: 'text',
        backgroundColor: 'background',
        lineHeight: 'tall'
      },
      a: {
        color: 'accent',
        _hover: {
          textDecoration: 'underline'
        }
      }
    }
  },
  components: {
    Button: {
      baseStyle: {
        fontWeight: 'bold',
        textTransform: 'uppercase',
        borderRadius: 'base',
        _focus: {
          boxShadow: 'none'
        }
      },
      variants: {
        solid: {
          bg: 'primary',
          color: 'background'
        },
        outline: {
          borderColor: 'primary',
          color: 'primary'
        }
      },
      defaultProps: {
        variant: 'solid'
      }
    }
  }
});