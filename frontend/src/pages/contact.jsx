import { Box, Flex, FormControl, FormLabel, Input, Textarea, Button, useToast } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { theme } from '@utilities/theme';

const ContactPage = () => {
  const { register, handleSubmit, formState: { errors }, reset } = useForm();
  const toast = useToast();

  const onSubmit = data => {
    console.log(data);
    toast({
      title: 'Message sent.',
      description: "We've received your message and will get back to you shortly.",
      status: 'success',
      duration: 9000,
      isClosable: true,
    });
    reset();
  };

  return (
    <Flex
      direction='column'
      align='center'
      justify='center'
      minH='100vh'
      bg={theme.colors.gray[50]}
      p={4}
    >
      <Box
        as='form'
        w={{ base: 'full', md: 'md' }}
        bg='white'
        boxShadow='xl'
        rounded='lg'
        p={8}
        onSubmit={handleSubmit(onSubmit)}
      >
        <FormControl isInvalid={errors.name} mb={4}>
          <FormLabel htmlFor='name'>Name</FormLabel>
          <Input id='name' placeholder='Your Name' {...register('name', { required: 'Name is required' })} />
        </FormControl>
        <FormControl isInvalid={errors.email} mb={4}>
          <FormLabel htmlFor='email'>Email</FormLabel>
          <Input id='email' placeholder='Your Email' type='email' {...register('email', { required: 'Email is required' })} />
        </FormControl>
        <FormControl isInvalid={errors.subject} mb={4}>
          <FormLabel htmlFor='subject'>Subject</FormLabel>
          <Input id='subject' placeholder='Subject' {...register('subject', { required: 'Subject is required' })} />
        </FormControl>
        <FormControl isInvalid={errors.message} mb={6}>
          <FormLabel htmlFor='message'>Message</FormLabel>
          <Textarea id='message' placeholder='Your Message' {...register('message', { required: 'Message is required' })} />
        </FormControl>
        <Button type='submit' colorScheme='blue' w='full'>Send Message</Button>
      </Box>
    </Flex>
  );
};

export default ContactPage;