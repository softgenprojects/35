import { Box, Container, Heading, Text, SimpleGrid, VStack, HStack, Icon } from '@chakra-ui/react';
import { MdTimeline } from 'react-icons/md';
import { customTheme } from '@utilities/theme';

const AboutPage = () => {
  return (
    <Container maxW='container.xl' py={{ base: 6, md: 10 }} px={{ base: 4, md: 8 }}>
      <VStack spacing={10} align='stretch'>
        <Box as='section'>
          <Heading as='h1' size='2xl' mb={4}>
            About Marko Kraemer
          </Heading>
          <Text fontSize='lg'>
            Hey, my name is Marko! I am 18 years old and I have been running software projects and companies since I was 13. I have scaled up a software development agency to 1+ million euros in revenue, and over the last few years, most of my time has been spent planning and managing custom software development projects.
          </Text>
        </Box>

        <Box as='section'>
          <Heading as='h2' size='xl' mb={4}>
            Career Milestones
          </Heading>
          <SimpleGrid columns={{ base: 1, md: 2 }} spacing={5}>
            <VStack spacing={3} align='start'>
              <HStack spacing={2}>
                <Icon as={MdTimeline} w={6} h={6} />
                <Text fontWeight='bold'>Founded SoftGen.ai</Text>
              </HStack>
              <Text>
                Created a comprehensive development solution that generates the complete codebase based on project requirements.
              </Text>
            </VStack>
            {/* More career milestones here */}
          </SimpleGrid>
        </Box>

        <Box as='section'>
          <Heading as='h2' size='xl' mb={4}>
            Philosophy on AI
          </Heading>
          <Text fontSize='lg'>
            The rise of GPT and AI is changing the economy - in 10 years it will be fundamentally different than it is today. Latest GPT models allow autonomous AI's to be deployed. Existing processes need to be rethought based on first-principle thinking in all industries; we will be part of that change for custom software development.
          </Text>
        </Box>
      </VStack>
    </Container>
  );
};

export default AboutPage;
